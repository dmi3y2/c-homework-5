﻿#include <iostream>
using namespace std;

class Vector
{
private:
    double x = 4;
    double y = 4;
    double z = 2;
    double L = sqrt(x * x + y * y + z * z);
public:
    Vector()
    {}
    void Show()
    {
        cout << "Vector coordinates: " << " X = " << x << " Y = " << y << " Z = " << z << "\n";
    }
    double Length()
    {
        return L;
    }
};

int main()
{
    Vector v;
    v.Show();
    v.Length();
   
    cout << "\n" << "Vector length: " << v.Length() << "\n";
}